import express from 'express';
const router = express.Router();

const items = [{ id: 1, task: "buy groceries" }, { id: 2, task: "clean room" }];

router.get('/', (req, res) => {
 res.send(items);
});

router.post('/', (req, res) => {
 let { id, task } = req.body;
 if (id || task) {
  items.push(req.body);
  return res.status(201).send({ id: Number(id), task });
 }
 else
  return res.status(400).send({ message: " This request does not have any of these properties in its object." })

});

router.get('/:id', async (req, res) => {
 let requestedId = req.params.id;
 let data = await items.find(item => item.id == requestedId);
 if (data == null || data == 'undefined') {
  return res.status(400).send({ message: "Cannot find this item!" })
 }
 else
  return res.status(201).send(data);

});


router.put('/:id', async (req, res) => {
 try {
  let id = req.params.id;
  let task = req.body.task;
  let data = await items.find(item => item.id == id);
  if (data == null || data == 'undefined') {
   return res.status(400).send({ message: "Cannot find this item!" })
  }
  else if (id == req.body.id) return res.status(201).send({ id: id, task: task });
  else return res.status(400).send({ message: " URL Id is not equal to input id!" });
 } catch (err) {
  return res.status(400).json({ err })
 }
});



router.delete('/:id', async (req, res) => {
 try {
  let pramsID = req.params.id;
  let index = await items.findIndex(item => item.id == pramsID);
  let data = await items.find(item => item.id == pramsID);

  if (data) {
   items.splice(index, 1);
   return res.status(201).send({ message: `Item ID = ${data.id} is removed.` });
  }
  else
   return res.status(400).send({ message: "Cannot find this item!" });

 } catch (err) {
  return res.status(400).json({ err });
 }
});



router.patch('/:id', async (req, res) => {
 try {
  let pramsID = req.params.id;
  let { task } = req.body;
  let data = await items.find(item => item.id == pramsID);
  if (data) {
   res.status(200).send({
    id: pramsID,
    task
   }
   );
  }
  else
   return res.status(400).send({ message: "Cannot update task because cannot find this item in the array!" });

 } catch (err) {
  return res.status(400).json({ err });
 }
});

export default router
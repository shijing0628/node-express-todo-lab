import express from 'express'
import itemRoutes from './src/items.js'

const app = express();
const port = 3000;

app.use(express.json());

// routes
app.use("/items", itemRoutes);

app.get("/", (req, res) => res.send("Hello world"));

app.listen(port, () => console.log(`API server ready on http://localhost:${port}`));
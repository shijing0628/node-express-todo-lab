node-express-todo-lab
Shijing(Brenda) Liu 2020-11-15

# Instruction details

gitLab Day2 Lab excerises

Port:3000 (localhost:3000)

`npm start`

or we can run

`npm run dev`

get result
![](2020-11-15-10-07-09.png)

post result
![](2020-11-15-10-09-29.png)

insert new item when get again
![](2020-11-15-10-10-45.png)

get specific product
![](2020-11-15-10-12-52.png)

Individual Practice

1. Update the POST /items endpoint to send back the status code used to refer to Bad Request, if the object sent in request does not have any of these properties in its object: id, task. Alongside the status code, you can send any valid JSON object.

success request:
![](2020-11-15-19-02-43.png)

bad request:
![](2020-11-15-19-04-48.png)

2.Update the GET /items/:id endpoint to return the actual item in the items array that has the same id as the one in the parameter :id.

![](2020-11-15-12-15-36.png)

![](2020-11-15-12-14-49.png)

3.Create a new endpoint for PUT /items/:id. The object given should have both properties (id, task) in it.
This endpoint should find the item in the array, and update the task property with what was provided in the request. It should send the appropriate status code if the item was updated, alongside the updated item. If the id is not found, it should send back the not found status code. If the id is not equal to the id in the URI, this should be considered a bad request, and send back the appropriate status code.

success request:
![](2020-11-15-14-23-58.png)

bad request:
![](2020-11-15-14-26-57.png)

4. Create a new endpoint with route DELETE /items/:id. This endpoint should remove the item that has the same id as the parameter, and send the appropriate status code.

success delete one item in items array:
![](2020-11-15-15-51-24.png)

Bad request for delete: input a run id in url
![](2020-11-15-15-52-29.png)

5.Create a new endpoint PATCH /items/:id. It should only need to accept a single property task, and it can change the value of the given item in the array, and send back with the appropriate status code and the updated item.

success request:
![](2020-11-15-18-46-52.png)

bad request:
![](2020-11-15-18-47-25.png)

gitflow for each step:
![](2020-11-15-18-52-38.png)
